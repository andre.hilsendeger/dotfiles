#!/usr/bin/python
# -*- mode: Python; tab-width: 2; indent-tabs-mode:nil; -*-          #
######################################################################
# Last-Updated: 2013-09-12                                           #
#   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >            #
#                                                                    #
# Filename: .offlineimap                                             #
# Description:                                                       #
# Custom python functions for the offlineimap config                 #
######################################################################

import re, os

# First apply these transformations on ALL folder names
def_trans  = [ ('[Google Mail]', 'Gmail') ]
# Then rename individuals
name_trans = [ ('INBOX', 'Inbox'),
               ('Gmail/Sent Mail', 'Gmail/Sent')]

# no_sync, applied after nametrans
no_sync = ['Gmail/All Mail', 'Gmail/Starred', 'Gmail/Important']

# sync_order, applied after nametrans.
# Folders unmentioned come afterwards sorted by cmp
sync_order = ['Inbox', 'Notes', 'Notes/TODO/' ]

def get_password(machine, login, port):
    s = "machine %s login %s port %s password ([^ ]*)\n" % (machine, login, port)
    p = re.compile(s)
    authinfo = os.popen("gpg -q --no-tty -d ~/.authinfo.gpg").read()
    return p.search(authinfo).group(1)

def sort_folders(a, b):
    return {
        (False, False): lambda: cmp(a, b),
        (True,  True ): lambda: cmp(sync_order.index(a), sync_order.index(b)),
        (True,  False): lambda: -1,
        (False, True ): lambda: 1
    }[(a in sync_order, b in sync_order)]()

def folder_filter(folder):
    return folder not in no_sync

name_trans_dict = dict(name_trans)
def rename_folder(folder):
    for pattern, replace in def_trans:
        folder = re.sub(re.escape(pattern), replace, folder)

    if folder in name_trans_dict: return name_trans_dict[folder]
    return folder

name_trans_dict_reverse = dict( [ (r,p) for p, r in name_trans ] )
def reverse_rename_folder(folder):
    # Needs to happen in reverse order
    if folder in name_trans_dict_reverse:
        folder = name_trans_dict_reverse[folder]

    for replace, pattern in def_trans:
        folder = re.sub(re.escape(pattern), replace, folder)

    return folder
