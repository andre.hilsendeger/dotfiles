#!/bin/bash
## -*- mode: Sh; tab-width: 2; indent-tabs-mode:nil; -*-            ##
######################################################################
## Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ##
## Last-Updated: 2013-08-06                                         ##
##   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >          ##
##                                                                  ##
## Filename: bu-out                                                 ##
## Description:                                                     ##
## bash-utils output functions                                      ##
######################################################################

_bu-lib-require str colors

## Color setup
######################################################################
export _bu_out_colors_on=${_bu_out_colors_on:-true};

export bu_error=${bu_error:-$bu_bred}
export bu_warning=${bu_warning:-$bu_yellow}
export bu_info=${bu_info:-$bu_blue}

# Colors a string according to the message type.
# $1: type
# $2: string
bu-out-colored-str () {
    local type=$1; shift
    local str="$1"
    $_bu_out_colors_on && eval str="\$bu_$type\$str\$bu_color_reset"
    echo -e "$str"
}

## Output functions.
######################################################################
export _bu_line=""

# For internal use. Color & format a message according to the message type.
# $1: type
# rest: message
_bu_out () {
    _bu_line=${_bu_line:-${BASH_LINENO[1]}}
    local tag=$1; shift
    tag=$(bu-out-colored-str $tag "[$(bu-str-to-upper $tag) $bu_script_name:$_bu_line]")
    echo -e "$tag" "$@"
    _bu_line=""
}
bu-out-echo ()    { _bu_out echo    "$@"; }
bu-out-info ()    { _bu_out info    "$@"; }
bu-out-warning () { _bu_out warning "$@"; }
bu-out-error ()   { _bu_out error   "$@" >&2; }

# Prints a separator line.
# $1: delimiter
# $2: number of lines
bu-out-line () {
    local delim=${1-#}; shift
    local lines=${1-1}
    for i in $(seq 1 $lines); do
        printf "%${COLUMNS}s" "" | tr ' ' "$delim"
    done
}

## Export functions
######################################################################
bu-export-funs \
    bu-out-colored-str \
    bu-out-echo \
    bu-out-error \
    bu-out-info \
    bu-out-line \
    bu-out-warning
