#!/bin/bash
## -*- mode: Sh; tab-width: 2; indent-tabs-mode:nil; -*-            ##
######################################################################
## Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ##
##  Last-Updated: 2015-10-17                                        ##
##    By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >         ##
##                                                                  ##
## Filename: load                                                   ##
## Description:                                                     ##
## Bash-utils loading of libs                                       ##
######################################################################

## Global variables
######################################################################
export bu_script=$(readlink -f "${BASH_SOURCE[1]}")
export bu_script_name=$(basename "$bu_script")
export bu_script_dir=$(cd $(dirname "$bu_script"); pwd)

export bu_lib_dir=${bu_lib_dir:-$(cd $(dirname "${BASH_SOURCE[0]}"); pwd)}
export bu_load="$bu_lib_dir/load"

## Common functions
######################################################################
# Get the user name from a path inside the home directory.
# $1: path
bu-user-from-home () {
    echo $(cd $(dirname "${1:-$0}"); pwd) | cut -d '/' -f 3
}

# Checks if an array contains an element.
# $1: element
# rest: array
bu-array-contains () {
    local elem=$1; shift
    local e
    for e in "$@"; do [[ "$e" == "$elem" ]] && return 0; done
    return 1
}

bu-fun-run-if-exists () {
    local fun=$1; shift
    local args="$@"
    [[ $(declare -f -F $fun) ]] && $fun $args
}

# Exports all functions supplied as arguments.
bu-export-funs () {
    for fun in $@; do
        export -f $fun
    done
}

## Export functions
######################################################################
bu-export-funs \
    bu-array-contains \
    bu-export-funs \
    bu-user-from-home

export bu_user=$(bu-user-from-home $_source)
export bu_tmp_dir=/tmp/$bu_user/$bu_script_name

[[ ! -f "$bu_tmp_dir" ]] && mkdir -p "$bu_tmp_dir"
[[ $(whoami) == 'root' ]] && [[ "$bu_user" != 'root' ]] && \
    chown -R $bu_user /tmp/$bu_user

## Load libs
######################################################################
_bu_loaded_libs=${_bu_loaded_libs:-""}
bu-load-libs () {
    libs=""
    if [[ "$1" == "all" ]] ; then
        libs="$(find $bu_lib_dir -name "bu-*" | sed "s:.*bu-::g")"
    else
        libs="$@"
    fi
    for lib in $libs; do
        local file="$bu_lib_dir/bu-$lib"
        if [[ ! -r $file ]] ; then
            echo "[bash-utils load Error] no such file '$file'" >&2
            exit 1
        fi

        # Make sure to load each lib only once. Especcially since the libs
        # may require another one.
        if ! $(bu-array-contains "$lib" "$_bu_loaded_libs"); then
            . $file
            _bu_loaded_libs="$_bu_loaded_libs $lib"
        fi
    done
}

_bu-lib-require () {
    bu-load-libs "$@"
}

bu-load-libs $@
