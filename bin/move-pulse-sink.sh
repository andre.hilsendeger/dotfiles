#!/bin/bash
## -*- mode: Sh; tab-width: 2; indent-tabs-mode:nil; -*-            ##
######################################################################
## From: http://askubuntu.com/a/113322/170687                       ##
##  Last-Updated: 2015-08-30                                        ##
##    By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >         ##
##                                                                  ##
## Filename: move-pulse-sink                                        ##
## Description:                                                     ##
##                                                                  ##
######################################################################
. ~/.bash-utils/load log
######################################################################

function main {
    local sink=$1

    bu-log-info "Setting pulse sink to $sink"
    pacmd set-default-sink $sink
    pacmd list-sink-inputs | grep index | while read line; do
        local input=$(echo $line | cut -f2 -d' ')
        bu-log-info "Moving sink input $input"
        pacmd move-sink-input $input $sink
    done
}

main "$@"
