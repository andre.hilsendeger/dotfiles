#!/bin/bash
# -*- mode: Sh; tab-width: 2; indent-tabs-mode:nil; -*-
lines=35

# search & web options
######################################################################
data_dir=~/.local/share/dmenu
browser="firefox"
search_url="https://duckduckgo.com/?q="
hist_size=100
search_hist="$data_dir"/.search_hist
web_hist="$data_dir"/.web_hist

## 'run' action
######################################################################
run_prompt='>'
run-get-items () {
    source ~/.profile
    dmenu_path
}
run_escape=true
run-execute () {
    local cmd="$@"
    eval "source ~/.profile && $cmd" &
}
## 'search' action
######################################################################
search_prompt='search:'
search-get-items () {
    cat $search_hist || echo ""
}
search-execute () {
    local query="$1"
    update-history "$search_hist" "$query"

    local url=$search_url${query// /+}
    $browser "$url" &
}
## 'web' action
######################################################################
web_prompt='www:'
web-get-items () {
    cat $web_hist || echo ""
}
web-execute () {
    local url="$@"
    update-history "$web_hist" "$url"
    $browser "$url" &
}
## Main
######################################################################
dmenu-exec () {
    local prompt="$1"
    dmenu  -p "$prompt" -l $lines -i 
}

update-history () {
    local file="$1"; shift
    local new="$1"
    if [[ "$new" != "" ]] ; then
        local tmp_file=$file.tmp
        mv "$file" "$tmp_file"
        echo "$new" > "$file"
        grep -v "^$new\$" "$tmp_file" | head -n $((hist_size-1)) >> "$file"
        rm "$tmp_file"
    fi
}

main () {
    local action="$1"; shift # if unkwon action just use 'run'
    [[ ! "$action" =~ web|search ]] && action=run

    mkdir -p "$data_dir"

    eval local prompt="\$${action}_prompt"
    eval local escape="\${${action}_escape:-false}"
    if $escape ; then
        escape='sed s:\([][&|'\'\"'[:space:]]\):\\\1:g'
    else
        escape='cat'
    fi
    local selected=$($action-get-items | dmenu-exec "$prompt" | $escape)

    if [[ "$selected" != "" ]] ; then
        $action-execute "$selected" &
        disown
    fi
}

main "$@"
