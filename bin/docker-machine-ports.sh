#!/bin/bash

VM_NAME=default

function rule-name {
    local port="$1"
    echo -n "tcp-port$port"
}    

function open-port {
    local port="$1"
    if [ -z "$port" ] ; then
        echo "You must specify a port" 1>2&
        exit 1
    fi

    VBoxManage controlvm "$VM_NAME" natpf1 "$(rule-name $port),tcp,,$port,,$port"
}

function open-list {

    for port in "$@"; do
        open-port "$port"
    done
    list
}

function remove-port {
    local port="$1"
    VBoxManage controlvm "$VM_NAME" natpf1 delete "$(rule-name $port)"
}

function remove-list {

    for port in "$@"; do
        remove-port "$port"
    done
    list
}

function list {
    VBoxManage showvminfo "$VM_NAME" --machinereadable | awk -F '[",]' '/^Forwarding/ { printf ("Rule %s host port %d forwards to guest port %d\n", $2, $5, $7); }'
}

function main {

    local cmd="$1"; shift
    case "$cmd" in
        "open")
            open-list "$@"
            ;;
        "remove")
            remove-list "$@"
            ;;
        "list")
            list
            ;;
        *)
            echo "Usage: $0 [list|open <port list>|remove <port list>]"
            exit 1
            ;;
    esac
}

main "$@"
