#!/bin/bash

config_path="$HOME/.config/monitor/config"
screens_path='/sys/class/drm/card'

######################################################################
# Setup options defined in config
######################################################################
declare -A card_name
declare -A xrandr_name
declare -A setup
declare -A primary
declare default_primary

######################################################################
# Screen functions
######################################################################
function error {
    echo "$@" >&2
}

function is-connected {
  local name
  name="$1"
  xrandr | grep -q "$name connected"
}

function detect-setup {
    local name connected_screens
    connected_screens=()
    for key in "${!xrandr_name[@]}"; do
        name="${xrandr_name[$key]}"
        is-connected "$name" && connected_screens+=("$key")
    done
    echo "${connected_screens[@]}"
}

function change-setup {

    local setup_name=$1; shift
    local order=${setup["$setup_name"]}
    local primary_screen=$default_primary

    if [[ "${primary[$setup_name]}" != "" ]] ; then
        primary_screen=${primary[$setup_name]}
    fi

    local prev_screen
    local xrandr_args=()
    local -A used_screens

    local old_IFS=$IFS
    IFS="|"
    read -ra lines <<< "$order"
    IFS=$old_IFS

    local first_in_line
    for l in "${lines[@]}"; do

        for name in $l; do

            # one entry can consist of screen duplicates
            local screen_names=($(echo -n "$name" | tr '+' ' '))
            name="${screen_names[0]}"

            local screen_args="--output ${xrandr_name[$name]} --auto"

            # primary ?
            if [ "$primary_screen" = "$name" ]; then
                screen_args="$screen_args --primary"
            fi

            if [ "$prev_screen" != "" ] ; then
                screen_args="$screen_args --right-of ${xrandr_name[$prev_screen]}"
            else
                # first in line
                if [ "$first_in_line" != "" ]; then
                    screen_args="$screen_args --below ${xrandr_name[$first_in_line]}"
                fi

                first_in_line=$name
            fi

            # duplicates
            for duplicate_name in "${screen_names[@]:1}"; do
                screen_args="$screen_args --output ${xrandr_name[$duplicate_name]} --same-as ${xrandr_name[$name]} --auto"
            done

            # rememer previous
            prev_screen=$name
            for screen_name in "${screen_names[@]}"; do
                used_screens[$screen_name]=yes
            done

            run-xrandr "$screen_args"
        done

        # clear prev screen at end of each line
        prev_screen=""
    done

    # disable not used screens
    for name in "${!xrandr_name[@]}"; do
        if [ "${used_screens[$name]}" = "yes" ] ; then
            continue
        fi

        local screen_args="--output ${xrandr_name[$name]} --off"

        run-xrandr "$screen_args"
    done
    
    feh --bg-fill Pictures/wallpaper-mountain.jpg
}

function run-xrandr {
    local xrandr_args="$@"
    echo xrandr $xrandr_args
    xrandr $xrandr_args
}

######################################################################
# Main
######################################################################
function exit-usage {
    error "Unknown argument: $1

     USAGE: $(basename $0) [-d|--detect|-v|--view-detect|-l|--list|-s|--set|<config-setup>]

       where <config-setup> is one of:"

    for key in "${!setup[@]}"; do
        echo "       - $key"
    done
    exit 1
}

function main {

    # get config
    local config="${config_path}_$(hostname -s)"
    if [ -r "$config" ]; then
        . "$config";
    else
        error "Config not found: '$config'"
    fi

    local setup_to_use
    case "$1" in
        -l|--list)
            for name in "${!setup[@]}"; do
                echo "$name"
            done
            exit 0
            ;;
        -d|--detect)
            setup_to_use=$(detect-setup)
            echo "Detected setup: '$setup_to_use'"
            ;;
        -v|--view-detect)
            detect-setup
            exit 0
            ;;
        -s|--set)
            shift
            setup_to_use="$@"
            ;;
    esac

    if [ "${setup[$setup_to_use]}" != "" ] ; then
        change-setup "$setup_to_use"
        exit 0
    fi

    exit-usage
}

main "$@"
