;-*- mode: emacs-lisp; tab-width: 2; indent-tabs-mode:nil; -*-

;;; Helper functions for keybindings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun custom/mode-map (mode)
  (let ((mode-map (custom/symbol-append mode "-map")))
    `(lambda (k f) (define-key ,mode-map k f))))

(defun custom/translate-key (from to)
  (define-key key-translation-map (read-kbd-macro from) (read-kbd-macro to)))

(defun custom/transalet-keys (translations)
  (dolist (trans translations)
    (funcall 'custom/translate-key (first trans) (second trans))))

(defun custom/add-key-to-map (add-func key exec-func)
  (funcall add-func (read-kbd-macro key) exec-func))

(defun custom/add-key-list (add-func list)
  (dolist (mapping list)
    (custom/add-key-to-map add-func (first mapping) (second mapping))))

(defun custom/add-key-lists (keys)
  (dolist (map-keys keys)
    (custom/add-key-list (first map-keys) (second map-keys))))

(defun custom/add-key-list-after-load (after list)
  (eval-after-load after
    `(custom/add-key-lists ,list)))

(defun custom/add-key-lists-after-load (lists)
  (dolist (list lists)
    (custom/add-key-list-after-load (first list) (second list))))

(defmacro custom/make-cmd (fun &rest args)
  `(lambda ()
     (interactive)
     (,fun ,@args)))
;;; Global module vars
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Key translations C-u <=> C-x
(setq custom/key-translations '(("C-u" "C-x")
                                ("C-x" "C-u")
                                ("M-u" "M-x")
                                ("M-x" "M-u")
                                ("C-?" "C-h")
                                ("C-h" "C-?")))
(custom/transalet-keys custom/key-translations)

;; Private keybindings
(setq custom/keys
      `((global-set-key
         (("RET"           reindent-then-newline-and-indent)
          ("M-x"           smex)

          ("C-c r"         revert-buffer)
          ("C-c f"         custom/format-buffer)
          ("C-c C-q a "    align-regexp)

          ("C-c SPC"       ace-jump-mode)
          ("C-x SPC"       ace-jump-mode-pop-mark)

          ("C-="           er/expand-region)
          ("C-!"           custom/delete-word)

          ("C-c M-u"       ,(custom/make-cmd upcase-word -1))
          ("C-c M-l"       ,(custom/make-cmd downcase-word -1))

          ("C-?"           backward-delete-char) ;; closer backward delete key
          ("M-h"           backward-kill-word)
          ("<backspace>"   backward-delete-char)

          ("C-c C-c"       comment-dwim)
          ("C-c C-q c"     comment-dwim)         ;; some mode already have C-c C-c
          ("C-x ."         repeat)               ;; emulate vim dot command
          ("C-x C-y"       yas-insert-snippet)   ;; insert yasnippet

          ("C-S-l"         color-theme-solarized-light) ;; light color theme
          ("C-S-d"         color-theme-solarized-dark)  ;; dark color theme

          ("C-x C-p"       previous-buffer)      ;; better buffer switching
          ("C-x C-n"       next-buffer)

          ("C-x o"         win-switch-dispatch)  ;; faster buffer switching
          ("C-S-p"         win-switch-up)
          ("C-S-n"         win-switch-down)
          ("C-S-f"         win-switch-right)
          ("C-S-b"         win-switch-left)

          ("C-M-S-p"       enlarge-window)
          ("C-M-S-n"       shrink-window)
          ("C-M-S-f"       enlarge-window-horizontally)
          ("C-M-S-b"       shrink-window-horizontally)

          ("C-c C-b r"     custom/block-align-region)
          ("C-c C-b b"     custom/block-align-region-keep-newlines)
          ("C-c C-b l"     custom/block-align-line)

          ("C-c C-q r"     custom/block-align-comment-region)
          ("C-c C-q b"     custom/block-align-comment-region-keep-newlines)
          ("C-c C-q l"     custom/block-align-comment-line)

          ;; ("C-+"           text-scale-increase)
          ;; ("C--"           text-scale-decrease)
          ("C-c i"         imenu)                   ;; jump to definition

          ("C-s"           isearch-forward-regexp)  ;; use regex
          ("C-r"           isearch-backward-regexp)
          ("C-M-s"         isearch-forward)
          ("C-M-r"         isearch-backward)

          ;; Multiple cursors
          ("C-c m"         mc/edit-lines)
          ("C->"           mc/mark-next-like-this)
          ("C-<"           mc/mark-previous-like-this)
          ("C-c C-<"       mc/mark-all-like-this)

          ;; iy-go-to-char (jump to char, vi like)
          ("C-S-s"         iy-go-to-char)
          ("C-S-r"         iy-go-to-char-backward)
          ("C-c ;"         iy-go-to-char-continue)
          ("C-c ,"         iy-go-to-char-continue-backward)

          ("<f1>"          help-command)
          ("<f5>"          mu4e)
          ("<f6>"          custom/load-calendar)))

        (,(custom/mode-map 'emacs-lisp-mode)
         (("C-c v"         eval-buffer)
          ("TAB"           lisp-complete-symbol)))

        (,(custom/mode-map 'isearch-mode)
         (("C-?"           isearch-delete-char)
          ("<backspace>"   isearch-delete-char)
          ))))
(custom/add-key-lists custom/keys)

(setq custom/keys-after-load
      '((mu4e
         `((,(custom/mode-map 'mu4e-main-mode)
            (("U"             custom/mu4e-quick-update-mail-and-index)
             ))
           (,(custom/mode-map 'mu4e-headers-mode)
            (("C-S-u"         custom/mu4e-quick-update-mail-and-index)
             ))
           (,(custom/mode-map 'mu4e-view-mode)
            (("C-S-u"         custom/mu4e-quick-update-mail-and-index)
             ))))
        (jabber-chat
         `((,(custom/mode-map 'jabber-chat-mode)
            (("C-x C-j C-b"   ,(custom/make-cmd
                                jabber-chat-display-more-backlog 100))
             ))))
        ))
(custom/add-key-lists-after-load custom/keys-after-load)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-keybindings)
