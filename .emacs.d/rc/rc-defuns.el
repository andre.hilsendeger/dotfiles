;;;-*- mode: emacs-lisp; tab-width: 2; indent-tabs-mode:nil; -*-   ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;;  Last-Updated: 2015-08-28                                      ;;;
;;;    By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >       ;;;
;;;                                                                ;;;
;;; rc-defuns description:                                         ;;;
;;; own elisp functions                                            ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Global module vars
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq custom/fill-length 70)

;;; Setup helper
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar custom/debug nil
  "Set to non-`nil' get better error messages on startup.")

(defun custom/do-require (pkg)
  "require `pkg' and display a message."
  (require pkg)
  (message (concat "rc loaded " (symbol-name pkg))))

(defun custom/require (&rest packages)
  "require all `packages', If `custom/debug' not enabeld suround it in a
   condition case, and display error. Otherwise error give a more detailed
   stack-trace."
  (dolist (pkg packages)
    (if custom/debug
        (custom/do-require pkg)
      (condition-case err
          (custom/do-require pkg)
        (error (display-warning 'rc (concat "failed: " (symbol-name pkg) "  "
                                            (prin1-to-string err)) :error))))))

(defun custom/minor-modes (val list)
  "Set all minor modes in `list' to `val' (de-/activate) them all."
  (dolist (mode list)
    (when (fboundp mode) (funcall mode val))))

(defun custom/add-hooks (mode hooks)
  "Add all `hooks' to `mode'."
  (dolist (hook hooks)
    (add-hook mode hook)))

(defun custom/symbol-append (mode str)
  "Append `str' to `mode' and return as symbol.
   f.Ex. 'c-mode \"-hook\" => 'c-mode-hook"
  (intern (concat (symbol-name mode) str)))

(defun custom/mode-hook (mode)
  "Returns the hook of `mode'. (Simply appends \"-hook\".)"
  (custom/symbol-append mode "-hook"))

(defun custom/add-hook-to-modes (modes hook)
  "Add `hook' to all `modes'."
  (dolist (mode modes)
    (add-hook (custom/mode-hook mode) hook)))

;;; String/buffer manipulation
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun custom/length (&rest rest)
  "Summed length of all the strings/lists in `rest'."
  (apply '+ (mapcar 'length rest)))

(defun custom/fill-str (delim &optional total &rest str)
  (let* ((total-len (or total
                        custom/fill-length))
         (str-len (or (apply 'custom/length str)
                      0))
         (rem-length (- total-len str-len)))
    (if (> rem-length 0)
        (make-string rem-length delim)
      "")))


(defun custom/horizontal-line (delim)
  (interactive "cDelim: ")
  (insert (custom/fill-str delim)))

(defun custom/str-trim (str)
  "Trim leading and tailing whitespace from STR."
  (replace-regexp-in-string "\\`[[:space:]]*\\|[[:space:]]*$" "" str))

(defun custom/aligned-str (pre &rest str &optional total)
  (let* ((l (car (last str)))
         (total (unless (stringp l) l))
         (str   (replace-regexp-in-string
                 "\\` ?\\| ?$" " "
                 (apply 'concat (remove-if-not 'stringp str)))))
    (concat pre str
            (custom/fill-str ?\s total pre str pre)
            pre)))

(defun custom/str-split-into-lines (length str keep-newlines)
  (let* ((s (custom/str-trim str))
         (len (custom/length s))
         (result nil))
    (while (> len 0)
      (let* ((new-line (and keep-newlines (string-match "\n" s)))
             (idx (cond ((and new-line (<= new-line length))
                         new-line)
                        ((<= len length)
                         len)
                        (t
                         (string-match "[[:space:]]+[^[:space:]]*$"
                                       (substring s 0 length))))))
        (setq result (cons (substring s 0 idx) result))

        (if (>= idx len)
            (setq s "")
          (setq s (substring s (or (1+ idx) len))))
        (setq len (custom/length s))
        (when (and new-line (= 0 len))
          (setq result (cons "" result)))))
    (reverse result)))

(defun custom/str-join (delim &rest str)
  "joins a list of strings `str' into one with `delim' as delimiter.
   `str' can be a list of strings, or many arguments, each a string."
  (when (listp (first str))
    (setq str (apply 'append str)))
  (apply 'concat (car str)
         (mapcar #'(lambda (s) (concat delim s))
                 (cdr str))))

(defun custom/block-align (str &optional border keep-newlines width)
  "Block align the string `str'.
   Optional arguments:
   - `border':        surround block with a border string if non-nil
   - `keep-newlines': existing newlines won't be removed if non-nil
                      (useful if part of the string is pre-formatted)
   - `width':         block width of the text. Using `custom/fill-length'
                      if nil."
  (let ((border (if (stringp border)
                    border
                  ""))
        (width  (if (numberp width)
                    width
                  custom/fill-length))
        lines)

    (unless keep-newlines
      (setq str (replace-regexp-in-string "[[:space:]]*\n" "" str)))

    (setq lines (custom/str-split-into-lines
                 (- custom/fill-length
                    (* 2 (1+ (custom/length border))))
                 str keep-newlines))
    (apply 'custom/str-join "\n"
           (mapcar '(lambda (line)
                      (if (string= border "")
                          line
                        (concat (custom/aligned-str border line))))
                   lines))))

(defun custom/block-align-region (start end &optional is-comment keep-newlines width)
  (interactive "r")
  (let* ((str (buffer-substring-no-properties start end))
         (str-rm-end-newlines (replace-regexp-in-string "\n$\\|\\`\n" "" str))
         (block-str (if is-comment
                        (custom/block-align-comment str-rm-end-newlines
                                                    keep-newlines width)
                      (custom/block-align str-rm-end-newlines nil
                                          keep-newlines width))))
    (when block-str
      (save-excursion
        (save-restriction
          (let ((start (if (string= "\n" (substring str 0 1))
                           (1+ start)
                         start))
                (end (if (string= "\n" (substring str (1- (custom/length str))))
                         (1- end)
                       end)))
            (goto-char start)
            (delete-region start end)
            (insert block-str)
            (indent-according-to-mode)))))))

(defun custom/block-align-region-keep-newlines (start end)
  (interactive "r")
  (custom/block-align-region start end nil t))

(defun custom/block-align-line ()
  (interactive)
  (custom/block-align-region (point-at-bol) (point-at-eol)))

(defun custom/block-align-comment (str &optional keep-newlines width)
  "Block aligns text with `custom/block-align', but uses the comment prefix
   as `border'. (Removes any previous comment characters from `str'.)"
  (let* ((pre (replace-regexp-in-string
               "[[:space:]]+$" "" (or comment-continue comment-start)))
         (pre-long pre)
         lines)

    (when (string-match (concat "\\`\\(" pre "+\\)") str)
      (setq pre-long (match-string 0 str)))

    (setq str (replace-regexp-in-string (concat pre-long "\\|" pre) "" str))
    (custom/block-align str pre-long keep-newlines width)))

(defun custom/block-align-comment-region (start end &optional keep-newlines width)
  (interactive "r")
  (custom/block-align-region start end t keep-newlines width))

(defun custom/block-align-comment-region-keep-newlines (start end)
  (interactive "r")
  (custom/block-align-comment-region start end t))

(defun custom/block-align-comment-line ()
  (interactive)
  (custom/block-align-comment-region (point-at-bol) (point-at-eol)))

(defun custom/file-name ()
  (file-name-nondirectory (file-name-sans-extension (buffer-file-name))))

(defun custom/mode-str-name ()
  (substring (symbol-name major-mode) 0 -5))

(defun custom/mode-line (&optional mode)
  (let ((mode-str (or mode (custom/mode-str-name))))
    (concat "-*- mode: "
            mode-str
            "; tab-width: 2; indent-tabs-mode:nil; -*-")))

(defun custom/insert-mode-line ()
  "Insert mode-line"
  (interactive)
  (save-excursion
    (beginning-of-buffer)
    (insert comment-start (custom/mode-line) comment-end "\n" )))

(defun custom/lorem ()
  "Insert 'Lorem ipsum dolor' paragraph at point."
  (interactive)
  (insert "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."))

(defun custom/delete-word ()
  "Delete word at point."
  (interactive)
  (let* ((p (point))
         (beg (+ p   (skip-syntax-backward "w_")))
         (end (+ beg (skip-syntax-forward  "w_"))))
    (kill-region beg end)))

;; Authorization / password retrieval
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun custom/read-file (file-name)
  "Returns the content of `file-name' as string."
  (with-temp-buffer
    (insert-file-contents file-name)
    (buffer-string)))

(setq auth-source-cache-expiry nil)
(defun custom/auth-password (user host &optional port)
  "Retrieve password using `auth-source-search'."
  (let* ((auth-info (car (auth-source-search :max 1
                                             :host host
                                             :user user
                                             :port (or port t)
                                             :require '(:secret)
                                             :create t)))
         (password   (plist-get auth-info :secret)))
    (when (functionp password)
      (setq password (funcall password)))
    password))

(defun custom/package-dir (pkg-name)
  "Get the elpa package directory of `pkg-name'."
  (let* ((pkg-desc (assoc pkg-name package-alist))
         (pkg-vers (package-desc-vers (cdr pkg-desc)))
         (pkg-vers-str (package-version-join pkg-vers))
         (pkg-name-str (symbol-name pkg-name)))
    (package--dir pkg-name-str pkg-vers-str )))


;; Run functions in background (non-blocking)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun custom/def-spawned-advice (func spawn-func)
  "Overrides `func' to be spawned in the background.
   `spawn-func' does the spawning"
  (eval
   `(defadvice ,func (around ,func act)
      (eval
       `(lexical-let ,(cons (ad-do-it ad-do-it)
                            (mapcar #'(lambda (arg)
                                        `(,(first arg) ',(second arg)))
                                    ad-arg-bindings))
                     (funcall spawn-func))))))

(defun custom/def-deferred-advice-start ()
  (deferred:$ (deferred:next (lambda () ad-do-it ))))
(defun custom/def-deferred-advice (func)
  (custom/def-spawned-advice func 'custom/custom/def-deferred-advice-start))

(defun custom/list-odd-idx (list &optional is-even)
  "Filters `list' so that only all elements with an odd index remain."
  (if list
      (if is-even
          (custom/list-odd-idx (cdr list) (not is-even))
        (cons (car list) (custom/list-odd-idx (cdr list)  (not is-even))))
    nil))

;;; Funcions used from external calls
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; emacs format buffer (used from outside commands)
(defun custom/format-buffer ()
  "Format the whole buffer. (untabify, indent, remove trailing spaces)."
  (interactive)
  (indent-region (point-min) (point-max) nil)
  (untabify (point-min) (point-max))
  (delete-trailing-whitespace))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-defuns)
