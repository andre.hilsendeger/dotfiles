;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Created: 2013-06-25                                            ;;;
;;; Last-Updated: 2013-09-16                                       ;;;
;;;   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ;;;
;;;                                                                ;;;
;;; Filename: rc-org                                               ;;;
;;; Description:                                                   ;;;
;;; Org mode config                                                ;;;
;;; http://newartisans.com/2007/08/using-org-mode-as-a-day-planner ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq custom/org-dir                    "~/org"
      custom/org-todo                   (concat custom/org-dir "/todo.org")
      custom/org-todo-private           (concat custom/org-dir "/priv-todo.org")
      org-agenda-files                  `(,custom/org-todo
                                          ,custom/org-todo-private)
      org-default-notes-file            (concat custom/org-dir "/notes.org")
      org-agenda-ndays                  7
      org-deadline-warning-days         14
      org-agenda-show-all-dates         t
      org-agenda-skip-deadline-if-done  t
      org-agenda-skip-scheduled-if-done t
      org-agenda-start-on-weekday       nil
      org-reverse-note-order            t
      org-fast-tag-selection-single-key 'expert
      org-remember-store-without-prompt t
      remember-annotation-functions     '(org-remember-annotation)
      remember-handler-functions        '(org-remember-handler)
      org-agenda-custom-commands   '(("d" todo "DELEGATED" nil)
                                     ("c" todo "DONE|DEFERRED|CANCELLED" nil)
                                     ("w" todo "WAITING" nil)
                                     ("W" agenda "" ((org-agenda-ndays 21)))
                                     ("A" agenda ""
                                      ((org-agenda-skip-function
                                        (lambda nil
                                          (org-agenda-skip-entry-if
                                           'notregexp "\\=.*\\[#A\\]")))
                                       (org-agenda-ndays 1)
                                       (org-agenda-overriding-header
                                        "Today's Priority #A tasks: ")))
                                     ("u" alltodo ""
                                      ((org-agenda-skip-function
                                        (lambda nil
                                          (org-agenda-skip-entry-if
                                           'scheduled 'deadline
                                           'regexp "\n]+>")))
                                       (org-agenda-overriding-header
                                        "Unscheduled TODO entries: "))))
      org-remember-templates  `((116 "* TODO %?\n  %u" ,custom/org-todo "Tasks")
                                (110 "* %u %?" ,org-default-notes-file "Notes")))

(defun custom/whitespace/org-mode-hook ()
  "Turn of long lines highlighting for org-mode.
   Otherwise links are always highlighted"
  (when (eq 'org-mode major-mode)
    (whitespace-toggle-options 'lines-tail)))

;; Needs to happen after whitespace-mode is loaded.
;; => Can't use the org-mode-hook.
(add-hook 'whitespace-mode-hook 'custom/whitespace/org-mode-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-org)
