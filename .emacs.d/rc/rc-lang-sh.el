;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Created: 2013-07-25                                            ;;;
;;; Last-Updated: 2013-07-25                                       ;;;
;;;   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ;;;
;;;                                                                ;;;
;;; Filename: rc-lang-sh                                           ;;;
;;; Description:                                                   ;;;
;;; shell script config (sh-mode)                                  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; no need for sh-case. key already bound and yasnippet works better.
(custom/add-key-to-map (custom/mode-map 'sh-mode) "C-c C-c" nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-lang-sh)
