;;;   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Created: 2013-06-21                                            ;;;
;;; Last-Updated: 2013-08-03                                       ;;;
;;;   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ;;;
;;;                                                                ;;;
;;; Filename: rc-ace-jump                                          ;;;
;;; Description:                                                   ;;;
;;; ace-jump-mode config                                           ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(eval-after-load "ace-jump-mode"
  '(ace-jump-mode-enable-mark-sync))

(setq ace-jump-mode-gray-background     t
      ace-jump-mode-scope               'frame
      ace-jump-word-mode-use-query-char t
      ace-jump-mode-submode-list        '(ace-jump-word-mode
                                          ace-jump-line-mode
                                          ace-jump-char-mode)
      )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-ace-jump)
