;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Created: 2013-06-02                                            ;;;
;;;  Last-Updated: 2015-10-17                                      ;;;
;;;    By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >       ;;;
;;;                                                                ;;;
;;; Filename: rc-style                                             ;;;
;;; Description:                                                   ;;;
;;; Emacs style:                                                   ;;;
;;; * modeline                                                     ;;;
;;; * dedfault highlighting modes                                  ;;;
;;; * color theme                                                  ;;;
;;; * set default font                                             ;;;
;;; * font lock update                                             ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(custom/require 'whitespace
                'idle-highlight-mode
                'color-theme)

;;; General stuff (lines/columns, highlighting, etc)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq
 color-theme-is-global          t
 font-lock-maximum-decoration   t
 inhibit-startup-message        t
 tab-width                      2
 indent-tabs-mode               nil
 uniquify-buffer-name-style     'forward
 visible-bell                   t)

(color-theme-solarized-dark)

(set-language-environment       "UTF-8")
(set-default 'indent-tabs-mode  nil)

;;; whitespace-mode show trailing spaces etc
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq
 whitespace-line-column         80
 whitespace-style               '(face trailing lines-tail tabs))
(add-hook 'prog-mode-hook 'whitespace-mode) ;; can't be global. jabber loses colors

(custom/minor-modes 1  custom/enabled-minor-modes)
(custom/minor-modes -1 custom/disabled-minor-modes)

;; Global style hook for *all* modes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun custom/global-style-hook ()
  (idle-highlight-mode)            ;; mark word under cursor
  (rainbow-mode)                   ;; display hex color codes
  )
(custom/add-hook-to-modes '(text-mode prog-mode xrdb-mode)
                          'custom/global-style-hook)

;; Fonts & theme colors
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun fontify-frame (frame)
  (set-frame-parameter frame 'font custom/font))
;;(fontify-frame nil)
(push 'fontify-frame after-make-frame-functions)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-style)
