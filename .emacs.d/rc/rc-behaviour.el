;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;;  Last-Updated: 2015-08-28                                      ;;;
;;;    By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >       ;;;
;;;                                                                ;;;
;;; rc-behaviour                                                   ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Set some general stuff
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq
 diff-switches                "-u"
 ediff-window-setup-function  'ediff-setup-windows-plain
 mouse-yank-at-point          t
 recentf-auto-cleanup         'never
 save-place-file              "~/.emacs.d/places"
 sentence-end-double-space    t

 ;; Backup Files in Specific  Directory
 make-backup-files            t
 version-control              t
 delete-old-versions          t
 backup-directory-alist       '((".*" . "~/.emacs_backups/"))
 )

(defalias 'yes-or-no-p 'y-or-n-p)

;; Load general packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(load "server")       ;; start emacs server
(unless (server-running-p) (server-start))

;; Some general hooks
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(custom/add-hooks 'text-mode-hook '(turn-on-auto-fill turn-on-flyspell))

(defvar custom/prog-modes nil
  "Custom prog-modes. Every mode in this list will run 'prog-mode-hook.
   (This doesn't always seem to happen...)")
(defun custom/run-prog-mode-hook ()
  (run-hooks 'prog-mode-hook))
(custom/add-hook-to-modes custom/prog-modes 'custom/run-prog-mode-hook)

(defun custom/prog-mode-hook ()
  (hs-minor-mode)
  (setq save-place t))

;; pretty lambdas, (this is from esk)
(defun custom/pretty-lambdas ()
  (font-lock-add-keywords
   nil `(("(?\\(lambda\\>\\)"
          (0 (progn (compose-region (match-beginning 1) (match-end 1)
                                    ,(make-char 'greek-iso8859-7 107))
                    nil))))))

;; from esk, but extended
(setq custom/font-lock-keywords nil)
(defun custom/watchwords-hook ()
  (unless custom/font-lock-keywords
    (setq custom/font-lock-keywords (append custom/watchwords
                                            (mapcar 'upcase custom/watchwords))))
  (font-lock-add-keywords
   nil `((,(concat "\\<\\(" (custom/str-join "\\|"
                                             custom/font-lock-keywords) "\\)")
          1 font-lock-warning-face t))))

(custom/add-hooks 'prog-mode-hook '(custom/pretty-lambdas
                                    custom/watchwords-hook
                                    custom/prog-mode-hook))

(defvar custom/no-font-lock-modes nil
  "Deactivate font-lock for modes in this list.")
(defun custom/no-font-lock-hook ()
  (font-lock-mode 0))
(custom/add-hook-to-modes custom/no-font-lock-modes 'custom/no-font-lock-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar custom/kill-modes-without-asking nil
  "Modes in this list will kill their processes without asking.")
(defun custom/kill-without-asking-hook()
  (set-process-query-on-exit-flag (get-buffer-process (current-buffer)) nil))
(custom/add-hook-to-modes custom/kill-modes-without-asking
                          'custom/kill-without-asking-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-behaviour)
