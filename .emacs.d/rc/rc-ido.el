;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Created: 2013-06-15                                            ;;;
;;; Last-Updated: 2013-06-15                                       ;;;
;;;   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ;;;
;;;                                                                ;;;
;;; Filename: rc-ido                                               ;;;
;;; Description:                                                   ;;;
;;; ido-mode config (just one of the best things for emacs)        ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require 'ido-ubiquitous)
(ido-mode t)
(ido-ubiquitous t)

(setq ido-enable-prefix                       nil
      ido-enable-flex-matching                t
      ido-auto-merge-work-directories-length  nil
      ido-create-new-buffer                   'always
      ido-use-filename-at-point               'guess
      ido-use-virtual-buffers                 t
      ido-handle-duplicate-virtual-buffers    2
      ido-max-prospects                       10
      )

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-ido)
