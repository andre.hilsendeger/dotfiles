;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Created: 2013-06-13                                            ;;;
;;; Last-Updated: 2013-06-15                                       ;;;
;;;   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ;;;
;;;                                                                ;;;
;;; Filename: rc-yasnippet                                         ;;;
;;; Description:                                                   ;;;
;;; Personal yasnippet config                                      ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; Load yasnippet
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq custom/yasnippet-path (concat custom/contrib-path "yasnippet/"))
(add-to-list 'load-path custom/yasnippet-path)
(require 'yasnippet)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq yas-snippet-dirs
      `("~/.emacs.d/snippets"                       ;; personal snippets
        ,(concat custom/yasnippet-path "snippets/") ;; the default collection
        )
      yas-wrap-around-region   t
      yas-prompt-functions    '(yas-completing-prompt
                                yas-ido-prompt)
      yas-choose-tables-first  nil                  ;; merge snippets for modes
      yas-triggers-in-field    t                    ;; allow recursive snippets
      yas-verbosity            2
      )
(yas-global-mode 1)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-yasnippet)
