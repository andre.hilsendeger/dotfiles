;;;-*- mode: emacs-lisp; tab-width: 2; indent-tabs-mode:nil; -*-   ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Last-Updated: 2013-10-30                                       ;;;
;;;   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ;;;
;;;                                                                ;;;
;;; constants description:                                         ;;;
;;; Some constants (user name etc)                                 ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;; emacs vars
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq user-full-name "Andre Hilsendeger"
      user-mail-address "Andre.Hilsendeger@gmail.com"

      browse-url-browser-function 'browse-url-generic
      browse-url-generic-program "luakit"
      browse-url-generic-args    '("-u")
      message-signature nil
      )

;;; custom vars
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq
 custom/default-dict         "english"
 custom/watchwords           '("fixme" "fix" "todo" "hack"
                               "nocommit" "note" "refactor")

 custom/font                 "Liberation Mono 15"

 custom/disabled-minor-modes       '(blink-cursor-mode
                                     menu-bar-mode
                                     tool-bar-mode
                                     scroll-bar-mode)

 custom/enabled-minor-modes        '(column-number-mode
                                     global-font-lock-mode
                                     global-hl-line-mode
                                     global-linum-mode
                                     global-rainbow-delimiters-mode
                                     iimage-mode
                                     line-number-mode
                                     pending-delete-mode
                                     show-paren-mode)

 ;; Kill those processes without asking
 custom/kill-modes-without-asking  '(comint-exec
                                     erlang-exec
                                     term-exec)
 ;; no font lock for those modes
 custom/no-font-lock-modes         '(comint-mode
                                     erlang-shell-mode
                                     jabber-chat-mode
                                     jabber-roster-mode)

 ;; add modes to prog-mode-hook (doesn't get executed otherwise...)
 custom/prog-modes                 '(c-mode
                                     c++-mode
                                     clojure-mode
                                     d-mode
                                     emacs-lisp-mode
                                     erlang-mode
                                     haskell-mode
                                     java-mode
                                     javascript-mode
                                     js-mode
                                     js2-mode
                                     lisp-mode
                                     lua-mode
                                     org-mode
                                     python-mode
                                     ruby-mode
                                     scheme-mode
                                     sh-mode
                                     ))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-constants)
