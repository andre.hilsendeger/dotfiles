;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Created: 2013-06-13                                            ;;;
;;; Last-Updated: 2013-06-13                                       ;;;
;;;  By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >         ;;;
;;;                                                                ;;;
;;; Filename: rc-lang-latex                                        ;;;
;;; Description:                                                   ;;;
;;; Personal LaTeX config                                          ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun custom/latex-hook ()
  (require 'auctex-autoloads)
  (require 'preview)
  ;; (require 'tex-mik)

  (setq TeX-auto-save t)
  (setq TeX-parse-self t)
  (setq TeX-PDF-mode t)

  (setq latex-run-command "pdflatex")
  (setq tex-dvi-view-command "okular")
  (setq-default TeX-master nil)
  (setq reftex-plug-into-AUCTeX t))

(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)
(add-hook 'LaTeX-mode-hook 'custom/latex-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-lang-latex)
