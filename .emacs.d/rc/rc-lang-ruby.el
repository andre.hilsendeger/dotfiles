;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Last-Updated: 2013-06-15                                       ;;;
;;;   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ;;;
;;;                                                                ;;;
;;; Filename: rc-lang-ruby                                         ;;;
;;; Description:                                                   ;;;
;;; Personal ruby config                                           ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Update load-path
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq rsense-home "/home/ahilsend/code/rsense")
(add-to-list 'load-path (concat rsense-home "/etc"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'rsense)
(require 'auto-complete)

(add-to-list 'ac-sources 'ac-source-rsense-method)
(add-to-list 'ac-sources 'ac-source-rsense-constant)

(defun custom/ruby-mode-hook ()
  (ruby-end-mode t)
  (local-set-key (kbd "C-c .") 'rsense-complete))
(add-hook 'ruby-mode-hook 'custom/ruby-mode-hook)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-lang-ruby)
