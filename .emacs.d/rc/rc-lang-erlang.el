;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Last-Updated: 2013-07-03                                       ;;;
;;;   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ;;;
;;;                                                                ;;;
;;; Filename: rc-lang-erlang                                       ;;;
;;; Description:                                                   ;;;
;;; personal erlang related settings.                              ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; set some options/paths
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq
 erlang-root-dir     "/usr/local/lib/erlang/lib/erlang"
 erlang-man-root-dir "/usr/local/lib/erlang/lib/erlang/doc"

 erl-options '("-sname" "emacs" "-instr" "erl -pa ebin deps/*/ebin")
 inferior-erlang-machine-options erl-options

 erlang-skel-mail-address user-mail-address
 )
;; "/opt/erlangR15B/lib/erlang/lib/tools-2.6.6.6/emacs"
(add-to-list 'exec-path "/usr/local/lib/erlang/bin")
(add-to-list 'load-path "/usr/local/lib/erlang/lib/tools-2.6.6.6/emacs")
(add-to-list 'load-path "~/code/distel/elisp")

;; all required packages
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(custom/require 'compile-helper-setup
                'erlang-start
                'erlang-flymake
                'erlang-eunit
                'tempo
                'w3m-load
                'distel)

(erlang-flymake-only-on-save)
(setq erlang-eunit-autosave t)

;; build helper
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq build-helper-files
      '(("rebar.config" .
         (lambda (dir file)
           (let ((conf "_tmpConf")
                 (compopts "_tmpOpts")
                 (deps "_tmpDeps")
                 (res "_tmpRes")
                 (absdir (expand-file-name dir)))
             (setq end
                   (inferior-erlang-send-command
                    (format
                     (concat "f(%s), f(%s), f(%s), f(%s), f(%s),"
                             "{ok, %s} = file:consult('%s/%s'),"
                             "%s = proplists:get_value(erl_opts, %s, []),"
                             "%s = proplists:get_value(deps_dir, %s, []),"
                             "lists:map(fun(%s2) ->"
                             "  [ true = code:add_path(%s3 ++ \"/ebin\") || %s3 <- filelib:wildcard(\"%s/\" ++ %s2 ++ \"/*\") ]"
                             "  end, %s),"
                             "%s = c('%s', %s),"
                             "f(%s), f(%s), %s.")
                     conf compopts conf res deps
                     conf absdir file
                     compopts conf
                     deps conf
                     deps
                     deps deps absdir deps
                     deps
                     res (buffer-file-name) compopts
                     conf compopts res)
                    nil)))))))

;; Own compiling function
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun custom/erlang-compile ()
  (interactive)
  (save-some-buffers)
  (inferior-erlang-prepare-for-input)
  (save-excursion
    (set-buffer inferior-erlang-buffer)
    (compilation-forget-errors))
  (compile-helper)
  (sit-for 0)
  (inferior-erlang-wait-prompt)
  (save-excursion
    (set-buffer inferior-erlang-buffer)
    (setq compilation-error-list nil)
    (set-marker compilation-parsing-end end))
  (setq compilation-last-buffer inferior-erlang-buffer))

(distel-setup)

;; Keybindings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(custom/add-key-list (custom/mode-map 'erlang-mode)
                     '(("M-t m"    tempo-template-erlang-module)
                       ("M-t l"    tempo-template-erlang-loop)
                       ("M-t c"    tempo-template-erlang-case)
                       ("M-t r"    tempo-template-erlang-receive)
                       ("M-1"      custom/erlang-compile)

                       ("M-t h"    tempo-template-erlang-large-header)
                       ("M-t f"    custom/erlang-insert-edoc)
                       ("M-t M-f"  tempo-template-erlang-fun-doc)))

(defun custom/erlang-shell-mode-hook ()
  "Keymap is nil before mode is started. So it's happening in a hook"
  (custom/add-key-list (custom/mode-map 'erlang-shell-mode)
                       '(("C-M-i"    erl-complete)
                         ("M-?"      erl-complete)
                         ("M-."      erl-find-source-under-point)
                         ;; ("M-*"  erl-find-source-unwind)
                         ("M-,"      erl-find-source-unwind))))
(add-hook 'erlang-shell-mode-hook 'custom/erlang-shell-mode-hook)

;; Add erlang hook
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun custom/erlang-mode-hook ()
  (imenu-add-to-menubar "imenu"))
(add-hook 'erlang-mode-hook 'custom/erlang-mode-hook)

;; Update template vars
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar skel-sep
  "%%--------------------------------------------------------------------")
(setq
 erlang-skel-separator
 "%%%-------------------------------------------------------------------")

;; Personal header
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(setq erlang-skel-copyright-comment '())
(setq erlang-skel-author-comment
      '(& "%%% @author " user-full-name " <" user-mail-address ">" n))
(setq erlang-skel-created-comment
      '(& "%%% @since " (funcall erlang-skel-date-function)
          n "%%% @end" n))
(setq erlang-skel-large-header
      '(o (erlang-skel-separator)
          (erlang-skel-include erlang-skel-copyright-comment
                               erlang-skel-author-comment)
          "%%%" n
          "%%% @doc Summary" p "." n
          "%%% " n
          "%%%" n
          (erlang-skel-include erlang-skel-created-comment)
          (erlang-skel-separator)
          (erlang-skel-include erlang-skel-small-header) n
          skel-sep n
          "%% Public exports" n
          skel-sep n
          "-export([" n>
          "])." n n
          skel-sep n
          "%% Public interface" n
          skel-sep n n n
          skel-sep n
          "%% Internal functions" n
          skel-sep n
          ))

;; Empty function comment
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar custom/erlang-skel-fun-doc
  '(skel-sep n
             "%% @doc" n
             "%% " p n
             "%% @end" n
             "%%" n
             "%% @spec" n
             "%% @end" n
             skel-sep n))

;; Create function comment from specs
;; from http://stackoverflow.com/questions/2560696/erlang-edoc-in-emacs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun custom/erlang-insert-edoc ()
  "Insert edoc."
  (interactive)
  (save-excursion
    (when (re-search-forward "^\\s *-spec\\s +\\([a-zA-Z0-9_]+\\)\\s *(\\(\\(.\\|\n\\)*?\\))\\s *->[ \t\n]*\\(.+?\\)\\." nil t)
      (let* ((beg (match-beginning 0))
             (funcname (match-string-no-properties 1))
             (arg-string (match-string-no-properties 2))
             (retval (match-string-no-properties 4))
             (args (split-string arg-string "[ \t\n,]" t)))
        (when (re-search-forward (concat "^\\s *" funcname "\\s *(\\(\\(.\\|\n\\)*?\\))\\s *->") nil t)
          (let ((arg-types (split-string (match-string-no-properties 1) "[ \t\n,]" t)))
            (goto-char beg)
            (insert "%%-------------------------------------------------------------------\n")
            (insert "%% @doc\n")
            (insert "%% Description\n")
            (insert "%% @end\n")
            (insert "%% @spec " funcname "(")
            (dolist (arg args)
              (insert (car arg-types) "::" arg)
              (setq arg-types (cdr arg-types))
              (when arg-types
                (insert ", ")))
            (insert ") ->\n")
            (insert "%%       " retval "\n")
            (insert "%% @end\n")
            (insert "%%-------------------------------------------------------------------\n")))))))

;; Add own templates to tempo
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun custom/erlang-skel-hook ()
  (add-to-list 'erlang-skel
               '("Function documentation" "fun-doc" custom/erlang-skel-fun-doc)))

;;erlang script templates
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defvar custom/skel-escript-header
  '("#!/usr/bin/env escript" n
    "%% -*- erlang -*-" n
    "%%! -smp enable -sname "
    (file-name-nondirectory buffer-file-name)
    " -mnesia debug verbose" n
    "-module(" (file-name-nondirectory buffer-file-name) ")." n))

(defvar custom/escript-skel-sep
  '("%%==================================================================="))

(defvar custom/skel-small-script
  '((erlang-mode)
    (erlang-skel-include custom/skel-escript-header) n n
    "usage() -> " n>
    "io:format(\"Usage: ~s" p " ~n\", " n>
    "[filename:basename(escript:script_name())])." n n n
    "main(_) -> " n>
    "usage()."n ))

(defvar custom/skel-large-script
  '((erlang-mode)
    (erlang-skel-include custom/skel-escript-header) n
    "-export([failure/1, failure/2])." n n
    "%% @doc" n
    "%% Specifies the available commands with a description." n
    "%% @end" n
    "commands() -> "n>
    "[{example" p" , \"Shows the usage for this skeleton\"}]." n> n n
    "%% @doc" n
    "%% Executes the commands defined in commands()." n
    "%% @end" n
    "execute_command(example, _Args) ->" n>
    "ok;" n>
    "execute_command(Cmd, _Args) ->" n>
    "failure(\"Not implemented: ~p\", [Cmd])." n n n
    (erlang-skel-include custom/escript-skel-sep) n
    "%% General script skeleton" n
    (erlang-skel-include custom/escript-skel-sep) n
    "%% @doc" n
    "%% Separates the command line arguments into \"command\" and arguments." n
    "%% Then calls the functions with the name \"command\" with these." n
    "%% Otherwise is displays the usage." n
    "%% @end" n
    "main([Command | Args]) ->" n>
    "case lists:keyfind(list_to_atom(Command), 1, commands()) of" n>
    "{Cmd, _Desc} ->" n>
    "ok = execute_command(Cmd, [Args]);" n>
    "false ->" n>
    "usage()," n>
    "failure(\"~nCommand unknown: ~p~n\", [Command])" n>
    "end;" n>
    "main(_) ->" n>
    "usage()," n>
    "ok." n n n
    "%% @doc" n
    "%% Auto-generates a help message from the {cmd, \"Desc\"} tuple list." n
    "%% @end" n
    "usage() -> " n>
    "CommandHelp = lists:foldl(" n>
    "fun ({Cmd, Desc}, Usage) ->" n>
    "Usage ++ io_lib:format(" n>
    "\"\\t~-20.w~s~n\", [Cmd, Desc])" n>
    "end, \"Commands:~n\", commands())," n>
    "io:format(\"Usage: ~s Command~n\" ++ CommandHelp, " n>
    "[filename:basename(escript:script_name())])." n n n
    "%% @doc" n
    "%% Prints the Msg to standard_error and exits." n
    "%% @end" n
    "failure(Msg) ->" n>
    "failure(Msg, [])." n>
    "failure(Msg, Args) ->" n>
    "io:format(standard_error, Msg, Args)," n>
    "halt(127)."
    ))

(add-hook 'erlang-load-hook 'custom/erlang-skel-hook)
(tempo-define-template "erlang-small-script" custom/skel-small-script)
(tempo-define-template "erlang-large-script" custom/skel-large-script)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-lang-erlang)
