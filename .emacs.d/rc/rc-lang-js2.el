;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Created: 2013-06-13                                            ;;;
;;; Last-Updated: 2013-09-19                                       ;;;
;;;   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ;;;
;;;                                                                ;;;
;;; Filename: rc-lang-js2                                          ;;;
;;; Description:                                                   ;;;
;;; Personal js2 config                                            ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(custom/add-key-list (custom/mode-map 'js2-mode)
                     '(("{"    paredit-open-curly)
                       ("}"    paredit-close-curly-and-newline)))

;; from esk
(font-lock-add-keywords
 'js2-mode `(("\\(function *\\)("
              (0 (progn (compose-region (match-beginning 1)
                                        (match-end 1) "\u0192")
                        nil)))))

(defun custom/js2-mode-hook ()
  (electric-indent-mode t)
  (setq js2-basic-offset 2
      js2-strict-inconsistent-return-warning nil
      js2-highlight-level 3
      js2-compiler-reserved-keywords-as-identifier t
      js2-allow-keywords-as-property-names t))
(add-hook 'js2-mode-hook 'custom/js2-mode-hook)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-lang-js2)
