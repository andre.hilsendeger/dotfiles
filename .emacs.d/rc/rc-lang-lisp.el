;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Last-Updated: 2013-07-27                                       ;;;
;;;   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ;;;
;;;                                                                ;;;
;;; Filename: rc-lang-lisp                                         ;;;
;;; Description:                                                   ;;;
;;; lisp config                                                    ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun custom/emacs-lisp-mode-hook ()
  (paredit-mode 1))

(custom/add-hooks 'emacs-lisp-mode-hook '(custom/emacs-lisp-mode-hook
                                          turn-on-eldoc-mode))

(custom/add-key-list (custom/mode-map 'emacs-lisp-mode)
                     '(("M-\\"  indent-for-tab-command)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-lang-lisp)
