;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Created: 2013-06-13                                            ;;;
;;;  Last-Updated: 2015-10-17                                      ;;;
;;;    By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >       ;;;
;;;                                                                ;;;
;;; Filename: rc-lang                                              ;;;
;;; Description:                                                   ;;;
;;; Load programming language specific stuff                       ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq
 ;; autoload for some languages, that won't load themselves
 custom/autoload-lang    '((d-mode        "\\.d[i]?\\'")
                           (LaTeX-mode    "\\.tex\\'")
                           (markdown-mode "\\.md\\'")
                           (snippet-mode  "\\.yasnippet\\'")
                           (xrdb-mode     "\\.Xresources\\'"))
 custom/direct-require   '(rc-lang-lisp)
 ;; Programming language specific configs
 custom/lazy-require     '(("clojure-mode"    rc-lang-clojure)
                           ("erlang"          rc-lang-erlang)
                           ("haskell-mode"    rc-lang-haskell)
                           ("python"          rc-lang-python)
                           ("ruby-mode"       rc-lang-ruby)
                           ("scala-mode"      rc-lang-scala)
                           ("sh-script"       rc-lang-sh)
                           ("tex-site"        rc-lang-latex)))

;; Setup autoloads and lazy config load
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(dolist (mode-list custom/autoload-lang)
  (let ((mode (nth 0 mode-list))
        (ext (nth 1 mode-list)))
    (autoload mode (symbol-name mode) nil t)
    (add-to-list 'auto-mode-alist `(,ext . ,mode))))

(dolist (l custom/lazy-require)
  (let ((after (first l))
        (rc    (second l)))
    (eval-after-load after
      `(custom/require (quote ,rc)))))

(apply 'custom/require custom/direct-require)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-lang)
