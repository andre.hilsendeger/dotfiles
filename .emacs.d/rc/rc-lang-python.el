;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-  ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >      ;;;
;;; Created: 2013-06-23                                            ;;;
;;; Last-Updated: 2013-07-01                                       ;;;
;;;   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ;;;
;;;                                                                ;;;
;;; Filename: rc-lang-python                                       ;;;
;;; Description:                                                   ;;;
;;; python specific emacs config                                   ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'load-path (concat custom/contrib-path "/emacs-for-python/"))
(custom/require 'epy-setup
                'epy-python
                'epy-completion
                'epy-nose
                ;; 'epy-editing    ;; For configurations related to editing [optional]
                ;; 'epy-bindings   ;; For my suggested keybindings [optional]
                )

(custom/add-key-list (custom/mode-map 'python-mode)
                     '(;; running tests
                       ("M-p o"    nosetests-one)
                       ("M-p m"    nosetests-module)
                       ("M-p a"    nosetests-all)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(provide 'rc-lang-python)
