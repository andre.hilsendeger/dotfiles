;;; -*- mode: Emacs-Lisp; tab-width: 2; indent-tabs-mode:nil; -*-

;;; Setup paths
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(setq custom/path "~/.emacs.d/"
      custom/rc-path (concat custom/path "rc/")
      custom/contrib-path (concat custom/path "contrib/")
      )

(setq load-path (append (list custom/rc-path
                              custom/contrib-path)
                        load-path))

;; Load everything
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(require 'rc-defuns)

(custom/require
  ;; general stuff
 'rc-package
 'rc-constants
 ;;'rc-style
 'rc-behaviour
 'rc-keybindings

 ;; configure some packages
 ;;'rc-ido
 'rc-lang
 ;;'rc-org
 ;;'rc-smex
 'rc-tramp
 'rc-uniquify
 'rc-win-switch ;; navigation between windows
 )

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(go-mode dockerfile-mode terraform-mode markdown-mode yaml-mode color-theme-solarized solarized-theme magit magit-tramp color-theme-sanityinc-solarized smart-operator rainbow-mode ido-yes-or-no idle-highlight-mode paredit smartparens xclip color-theme win-switch idle-highlight ido-ubiquitous smex)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
