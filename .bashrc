## -*- mode: Sh; tab-width: 2; indent-tabs-mode:nil; -*-            ##
######################################################################
## Author: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >        ##
## Last-Updated: 2013-07-22                                         ##
##   By: Andre Hilsendeger < Andre.Hilsendeger@gmail.com >          ##
##                                                                  ##
## Filename: .bashrc                                                ##
## Description:                                                     ##
## bash config                                                      ##
######################################################################
. ~/.env_shared
source-configs   ~/.aliases
source-host-file ~/.bashrc

source-file      /etc/bash_completion

### Prompt
#######################################################################
if [ $(id -u) -eq 0 ]; then
    USERNAME_COLOR="\033[1;31m"
else
    USERNAME_COLOR="\033[1;34m"
fi
AT_COLOR="\033[0;34m"
HOSTNAME_COLOR="\033[0;32m"
PATH_COLOR="\033[0;33m"
NO_COLOR="\033[0m"

PS2="🐳 "
PS1="
[ \D{%a %d %b %y %T} | ${debian_chroot:+($debian_chroot)}$USERNAME_COLOR\u$AT_COLOR @ $HOSTNAME_COLOR\h $NO_COLOR]
$PATH_COLOR\w$NO_COLOR
$PS2"
